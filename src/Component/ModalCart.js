import React, { Component } from "react";
import { Modal } from "antd";

export default class ModalCart extends Component {
  render() {
    let tongTien = this.props.Cart.reduce((accumulator, currentValue) => {
      return accumulator + currentValue.quantity * currentValue.giaBan;
    }, 0);
    return (
      <div>
        <Modal
          title="Giỏ hàng"
          centered
          visible={this.props.OpenModal}
          onOk={() => {
            this.props.handleCloseModal();
          }}
          onCancel={() => {
            this.props.handleCloseModal();
          }}
          width={1000}
        >
          {" "}
          <div className="text-danger">
            {tongTien == 0 ? "Không có sản phẩm trong giỏ hàng" : ""}
          </div>
          <table className="table">
            <thead>
              <tr>
                <th scope="col" className="text-center">
                  Mã sản phẩm
                </th>
                <th scope="col" className="text-center">
                  Hình ảnh
                </th>
                <th scope="col" className="text-center">
                  Tên sản phẩm
                </th>
                <th scope="col" className="text-center">
                  Số lượng
                </th>
                <th scope="col" className="text-center">
                  Đơn giá
                </th>
                <th scope="col" className="text-center">
                  Thành tiền
                </th>
                <th scope="col" className="text-center"></th>
              </tr>
            </thead>
            <tbody>
              {this.props.Cart.map((item, i) => {
                let price = new Intl.NumberFormat("vn-VN", {}).format(
                  item.giaBan
                );
                let totalPrice = new Intl.NumberFormat("vn-VN", {}).format(
                  item.quantity * item.giaBan
                );
                return (
                  <tr key={i}>
                    <th scope="row" className="text-center">
                      {item.maSP}
                    </th>
                    <td className="text-center" style={{ width: "100px" }}>
                      <img src={item.hinhAnh} style={{ width: "100%" }}></img>
                    </td>
                    <td className="text-center">{item.tenSP}</td>
                    <td className="text-center">
                      <button
                        className="btn btn-danger mr-2"
                        onClick={() => {
                          this.props.handleReduceQuantity(item);
                        }}
                      >
                        {item.quantity == 1 ? "Xóa" : "-"}
                      </button>
                      {item.quantity}
                      <button
                        className="btn btn-success ml-2"
                        onClick={() => {
                          this.props.handleIncreaseQuantity(item);
                        }}
                      >
                        +
                      </button>
                    </td>
                    <td className="text-center">{price}</td>
                    <td className="text-center">{totalPrice}</td>
                    <td className="text-center">
                      <button
                        className="btn btn-danger"
                        onClick={() => {
                          this.props.handleDeleteItem(item, i);
                        }}
                      >
                        Xóa
                      </button>
                    </td>
                  </tr>
                );
              })}
              <tr>
                <td
                  colSpan="5"
                  className="font-weight-bold"
                  style={{ fontSize: "18px" }}
                >
                  Tổng tiền
                </td>
                <td className="font-weight-bold" style={{ fontSize: "18px" }}>
                  {new Intl.NumberFormat("vn-VN", {}).format(tongTien)} VND
                </td>
              </tr>
            </tbody>
          </table>
        </Modal>
      </div>
    );
  }
}
