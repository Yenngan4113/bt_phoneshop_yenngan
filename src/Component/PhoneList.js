import React, { Component } from "react";

import PhoneItem from "./PhoneItem";

export default class PhoneList extends Component {
  render() {
    return (
      <div className="container">
        <div className="row justify-content-center">
          {this.props.PhoneList.map((item, index) => {
            return (
              <PhoneItem
                key={index}
                Item={item}
                handleShowDetail={this.props.handleShowDetail}
                handleAddItemtoCart={this.props.handleAddItemtoCart}
              />
            );
          })}
        </div>
      </div>
    );
  }
}
