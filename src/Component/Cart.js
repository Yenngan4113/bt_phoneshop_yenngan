import React, { Component } from "react";

export default class Cart extends Component {
  render() {
    const handleQuantity = (accumulator, currentValue) => {
      return accumulator + currentValue.quantity;
    };
    let totalQuantity = this.props.Cart.reduce(handleQuantity, 0);
    return (
      <div
        className="text-right mr-5 text-danger font-weight-bold"
        style={{ fontSize: "18px" }}
      >
        <button
          className="btn btn-outline-danger"
          onClick={() => {
            this.props.handleOpenModal();
          }}
        >
          Giỏ hàng ({totalQuantity})
        </button>
      </div>
    );
  }
}
