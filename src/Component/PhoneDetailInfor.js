import React, { Component } from "react";

export default class PhoneDetailInfor extends Component {
  render() {
    let price = new Intl.NumberFormat("vn-VN", {}).format(
      this.props.ChosenPhone.giaBan
    );

    return (
      <div className="row">
        <div className="col-4 my-3">
          <img
            src={this.props.ChosenPhone.hinhAnh}
            style={{ width: "100%" }}
          ></img>
        </div>
        <div className="col-8 my-5">
          <h3>Thông số kỹ thuật</h3>
          <table className="table">
            <thead></thead>
            <tbody>
              <tr>
                <th scope="row">Màn hình</th>
                <td colSpan="3">{this.props.ChosenPhone.manHinh}</td>
              </tr>
              <tr>
                <th scope="row">Hệ điều hành</th>
                <td colSpan="3">{this.props.ChosenPhone.heDieuHanh}</td>
              </tr>
              <tr>
                <th scope="row">Camera trước</th>
                <td colSpan="3">{this.props.ChosenPhone.cameraTruoc}</td>
              </tr>
              <tr>
                <th scope="row">Camera sau</th>
                <td colSpan="3">{this.props.ChosenPhone.cameraSau}</td>
              </tr>
              <tr>
                <th scope="row">Ram</th>
                <td colSpan="3">{this.props.ChosenPhone.ram}</td>
              </tr>
              <tr>
                <th scope="row">Rom</th>
                <td colSpan="3">{this.props.ChosenPhone.rom}</td>
              </tr>
              <tr>
                <th scope="row">Giá bán</th>
                <td colSpan="3">{price} VND</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
