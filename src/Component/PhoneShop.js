import React, { Component } from "react";
import PhoneList from "./PhoneList";
import { data } from "./dataPhone";
import PhoneDetailInfor from "./PhoneDetailInfor";
import Cart from "./Cart";
import ModalCart from "./ModalCart";

export default class PhoneShop extends Component {
  state = {
    PhoneList: data,
    chosenPhone: {},
    cart: [],
    isOpenModal: false,
  };
  handleShowDetail = (item) => {
    let cloneChosenPhone = { ...item };
    this.setState({ chosenPhone: cloneChosenPhone });
  };
  handleAddItemtoCart = (item) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((cartItem) => {
      return cartItem.maSP == item.maSP;
    });
    if (index != -1) {
      cloneCart[index].quantity++;
    } else {
      let newItem = { ...item, quantity: 1 };
      cloneCart.push(newItem);
    }

    this.setState({ cart: cloneCart });
  };
  handleOpenModal = () => {
    this.setState({ isOpenModal: true });
  };
  handleCloseModal = () => {
    this.setState({ isOpenModal: false });
  };
  handleDeleteItem = (item, id) => {
    let cloneCart = [...this.state.cart];

    let index = cloneCart.findIndex((cartItem, i) => {
      return cartItem.maSP == item.maSP;
    });

    if (index != -1) {
      cloneCart.splice(index, 1);
    }
    this.setState({ cart: cloneCart });
  };
  handleIncreaseQuantity = (item) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((cartItem) => {
      return cartItem.maSP == item.maSP;
    });
    if (index != -1) {
      cloneCart[index].quantity++;
    }
    this.setState({ cart: cloneCart });
  };
  handleReduceQuantity = (item) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((cartItem) => {
      return cartItem.maSP == item.maSP;
    });
    if (index != -1 && cloneCart[index].quantity > 1) {
      cloneCart[index].quantity--;
    } else {
      cloneCart.splice(index, 1);
    }
    this.setState({ cart: cloneCart });
  };
  render() {
    return (
      <div>
        <h1 className="text-center bg-dark text-white py-2">Phone Shop App</h1>
        <Cart Cart={this.state.cart} handleOpenModal={this.handleOpenModal} />
        <ModalCart
          OpenModal={this.state.isOpenModal}
          handleCloseModal={this.handleCloseModal}
          Cart={this.state.cart}
          handleDeleteItem={this.handleDeleteItem}
          handleIncreaseQuantity={this.handleIncreaseQuantity}
          handleReduceQuantity={this.handleReduceQuantity}
        />
        <PhoneList
          PhoneList={this.state.PhoneList}
          handleShowDetail={this.handleShowDetail}
          handleAddItemtoCart={this.handleAddItemtoCart}
        />
        {Object.getOwnPropertyNames(this.state.chosenPhone).length != 0 && (
          <PhoneDetailInfor ChosenPhone={this.state.chosenPhone} />
        )}
      </div>
    );
  }
}
