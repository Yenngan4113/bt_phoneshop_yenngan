import React, { Component } from "react";

export default class PhoneItem extends Component {
  render() {
    return (
      <div className="col-4">
        <div className="card ">
          <img
            className="card-img-top"
            src={this.props.Item.hinhAnh}
            alt="Card image cap"
          />
          <div className="card-body">
            <h5 className="card-title">{this.props.Item.tenSP}</h5>
          </div>
          <div className="card-body">
            <button
              className="btn btn-success mr-3"
              onClick={() => {
                this.props.handleShowDetail(this.props.Item);
              }}
            >
              Xem chi tiết
            </button>
            <button
              className="btn btn-danger"
              onClick={() => {
                this.props.handleAddItemtoCart(this.props.Item);
              }}
            >
              Thêm giỏ hàng
            </button>
          </div>
        </div>
      </div>
    );
  }
}
