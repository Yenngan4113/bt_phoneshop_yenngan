import logo from "./logo.svg";
import "./App.css";
import { Fragment } from "react";
import PhoneShop from "./Component/PhoneShop";
import "antd/dist/antd.css";

function App() {
  return (
    <Fragment>
      <PhoneShop />
    </Fragment>
  );
}

export default App;
